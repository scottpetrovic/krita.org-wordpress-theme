 <div class="row">
                <div class="col-md-2" id="footer-transition-image"></div>            
            </div>
            
            <footer class="row">
                <div class="col-md-2">
                    <h5>Software</h5>
                    <ul>
                        <li><a href="#">Krita Desktop</a></li>
                        <li><a href="#">Krita Gemini</a></li>
                        <li><a href="#">Krita Studio</a></li>
                        <li><a href="#">Features</a></li>
                        <li><a href="#">Gallery</a></li>
                        <li><a href="#">Report a Bug</a></li>
                    </ul>
                </div>
                
                 <div class="col-md-2">
                    <h5>Education</h5>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Tutorials</a></li>
                        <li><a href="#">Documentation</a></li>
                        <li><a href="#">Training</a></li>
                        <li><a href="#">Resources</a></li>
                    </ul>
                </div>
                
                <div class="col-md-2">
                   <h5>Foundation</h5>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Press</a></li>
                        <li><a href="#">Donate</a></li>
                        <li><a href="#">Get Involved</a></li>
                        <li><a href="#">Shop</a></li>
                    </ul>
                </div>
                
                <div id="socialmedia" class="col-md-6">                
                    <a href="http://krita-free-art-app.deviantart.com/" target="_blank"><img src="<?php echo bloginfo('template_directory')?>/images/social-deviantart.png" alt="" /></a>
                    <a href="https://www.facebook.com/pages/Krita-Foundation/511943358879536" target="_blank"><img src="<?php echo bloginfo('template_directory')?>/images/social-facebook.png" alt="" /></a>
                    <a href="#" target="_blank"><img src="<?php echo bloginfo('template_directory')?>/images/social-googleplus.png" alt="" /></a>
                    <a href="https://twitter.com/Krita_Painting" target="_blank"><img src="<?php echo bloginfo('template_directory')?>/images/social-twitter.png" alt="" /></a>
                    
                    <a class="loginlink" href="<?php echo get_bloginfo('url'); ?>/wp-admin/" >Admin log in</a>
                </div>
                
            </footer>
            
            
        </div> <!-- end container -->
        

       
        <!-- can remove jquery eventually since wordpress auto adds it -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
        <script src="<?php echo bloginfo('template_directory')?>/js/bootstrap.min.js"></script>
  


      <!-- TODO: put google analytics code down here -->
    </body>
</html>