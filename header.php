<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <title>
            <?php
            // Print the <title> tag based on what is being viewed.
            global $page, $paged;
        
            wp_title( '|', true, 'right' );
        
            // Add the blog name.
            bloginfo( 'name' );
        
            // Add the blog description for the home/front page.
            $site_description = get_bloginfo( 'description', 'display' );
            if ( $site_description && ( is_home() || is_front_page() ) )	echo " | $site_description";
                
            ?>
        </title>
        
        <meta charset="<?php echo bloginfo('charset'); ?>"  />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Krita.org">
        <meta name="keywords" content="Krita, drawing, painting, concept, art">
        <meta name="author" content="Krita Foundation">

        <link href="<?php echo bloginfo('template_directory')?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo bloginfo('template_directory')?>/css/bootstrap-theme.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
        <link href='http://fonts.googleapis.com/css?family=Just+Me+Again+Down+Here' rel='stylesheet' type='text/css'>
        
        
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
            
        
        
        <!--TODO: fav icon to go here -->
    </head>
    <body>
 
    
        <!--navigation -->
        <div class="navbar" role="navigation">
         
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand visible-xs" href="<?php echo get_bloginfo('url'); ?>" class="brandingPadding"><img src="<?php echo bloginfo('template_directory')?>/images/krita-logo.png" alt="Krita Logo" /></a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="hidden-xs"><a href="<?php echo get_bloginfo('url'); ?>" class="brandingPadding"><img src="<?php echo bloginfo('template_directory')?>/images/krita-logo.png" alt="Krita Logo" /></a></li>
                <li><a href="#features">Features</a></li>
                <li><a href="#about">Download</a></li>
                <li><a href="#contact">About</a></li>   
                <li><a href="#contact">Learn</a></li>  
                <li><a href="#contact">Get Involved</a></li>  
                <li><a href="#contact">Support Us</a></li>                      
              </ul>
            </div><!--/.nav-collapse -->
        </div>
          
        <!-- main content -->
        
        <div class="container" >
            
                <!-- slim area for header and breadcrumb navigation -->
                <div id="slim-container" class="row">
                    <div id="slim-background" class="col-md-12" >
                        <!-- parent page title, otherwise gets the regular title -->
                        <h2><?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?></h2>                    
                    </div>                    
                </div>
                <div id="breadcrumb-nav" class="row">
                    <ul>
                        <li class="active"><a href="#">Features</a></li>
                        <li><a href="#">Gallery</a></li>
                        <li><a href="#">Artist Interviews</a></li>
                    </ul>
                </div>
                
                
                